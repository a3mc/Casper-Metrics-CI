#!/usr/bin/env bash

APP=$1
TIER=$2
STATUS=$3
WEBHOOK=$4

case $STATUS in
    "success" )
    EMBED_COLOR="3066993"
    STATUS_MESSAGE="Passed"
    AVATAR="https://gitlab.com/intellekt89/public/discord/-/raw/main/gitlab.png?inline=false"
    ;;

    "failure" )
    EMBED_COLOR="15158332"
    STATUS_MESSAGE="Failed"
    AVATAR="https://gitlab.com/intellekt89/public/discord/-/raw/main/gitlab.png?inline=false"
    ;;
esac

COMMIT_SUBJECT="$(git log -1 "$CI_COMMIT_SHA" --pretty="%s")"
COMMIT_MESSAGE="$(git log -1 "$CI_COMMIT_SHA" --pretty="%b" | sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g')"

WEBHOOK_DATA='{
  "username": "",
  "avatar_url": "'"${AVATAR}"'",
  "content": "'"${GITLAB_USER_NAME}"' развернул новую версию '"${APP}"' ('"${TIER}"') '"${CI_ENVIRONMENT_URL}"'",
  "embeds": [{
    "color": '$EMBED_COLOR',
    "title": "Изменения в новой версии: '"$COMMIT_SUBJECT"'",
    "description": "Подробности об изменениях: '"$COMMIT_MESSAGE"'"
  }]
 
}'

(curl --fail --progress-bar -A "GitlabCI-Webhook" -H Content-Type:application/json -H X-Author:Dubalda#3996 -d "${WEBHOOK_DATA}" "${WEBHOOK}" \
  && echo -e "\\n[Webhook]: Successfully sent the webhook.") || echo -e "\\n[Webhook]: Unable to send webhook."
